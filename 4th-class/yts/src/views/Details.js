import React, {Component} from "react";
import axios from 'axios';
class Details extends Component{
    state={
        movie:{}
    }
    componentDidMount(){
        let id = this.props.match.params.id;
        axios.get('https://yts.lt/api/v2/movie_details.json?movie_id='+id)
            .then((res)=>{
                this.setState({
                    movie:res.data.data.movie
                })
            });
        console.log(this.props.match.params.id);
    }
    render() {
        return(
            <div>
                <h2>{this.state.movie.title}</h2>
                <img src={this.state.movie.large_cover_image} alt=""/>

            </div>
        )
    }
}

export default Details;