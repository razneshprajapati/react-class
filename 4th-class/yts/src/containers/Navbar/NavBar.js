/*import React, {Component} from 'react';
import Logo from "../../components/Logo/Logo";

const NavBar = (props) => {
    return (
        <nav>
            <Logo logoname={props.navCheck}/>
            <NavItems navItems={props.navCheck}/>

        </nav>
    )
};

class NavBar extends Component{
    render() {
        return (
            <nav>
                

                <Logo logoname={this.props.navItems}/>
                <NavItems navItems={this.props.navItems}/>
            </nav>
        )
    }
}

const NavItems=(props)=>{
    return(
         <div className={'nav-items'}>
         {props.navCheck}
            
        </div>
    )
};

const NavItem=(props)=>{
    return(
        <div className={'nav-item'}>
            {props.label}
        </div>
    )
};

export default NavBar;

*/

import React,{Component} from 'react';
import Logo from "../../components/Logo/Logo";

class NavBar extends Component {
    render(){
        return (
            <nav>

                <Logo label={'Ram G'}></Logo>
                <hr className={'line'}/>
                <NavItems navItems={this.props.navItems}/>
            </nav>
        )
    }
};

const NavItems= (props)=>{
    return (
        <div className={'nav-items'}>
            {props.navItems.map((navItem)=>(
                <NavItem key={navItem} label={navItem}/>
            ))}
        </div>
    )
};

const NavItem= (props)=>{
    return (
        <div className="nav-item">{props.label.label}</div>
    )
}
export default NavBar;