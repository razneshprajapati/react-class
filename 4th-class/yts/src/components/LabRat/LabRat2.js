import React, {Component} from 'react';

class LabRat2 extends Component {
    state={
        email:'',
        name:'',
        age:'',
    };
    inputHandler=e=>{
        console.log(e.target.value)
        this.setState({
            [e.target.name]:e.target.value
        })
    }
    render() {
        return (
            <div>
                <input type='text' onChange={(e)=>(
                    this.inputHandler(e)
                )}
                       placeholder={"Enter a email"} name={"email"}/>
                <input type='text' onChange={(e)=>(
                    this.inputHandler(e)
                )}
                       placeholder={"Enter a name"} name={"name"}/>
                <input type='text' onChange={(e)=>(
                    this.inputHandler(e)
                )}
                       placeholder={"Enter a age"} name={"age"}/>

                <h1>{this.state.email} {this.state.name} {this.state.age}</h1>
            </div>
        );
    }
}

export default LabRat2;