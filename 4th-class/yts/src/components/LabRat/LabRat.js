import React, {Component} from 'react';

class LabRat extends Component{
    state={
        value : 'sth',
        multi : 'false',
        color : 'red',
        visible : 'true'
    }

    valueChangerHandler=()=>{
        this.setState({
            value:'change value after clicking'
        })
    }

    colorHandler=()=>{
        this.setState({
            color : 'blue'
        })
    }

    toogleBoxHandler=()=>{
        this.setState({
            visible : !this.state.visible
        })
    }

    render() {
        let visibile=(<div style={style}>I am a box</div>);
        return (
            <div>
               {/* <div id="box" style={{height:'100px',
                    width:'200px',
                    backgroundColor : this.state.color}}>Color changing</div>*/}

                {this.state.visible?visibile:""}
                <button onClick={this.toogleBoxHandler}>Click to change</button>

            </div>
        );
    }
}

const style={
    height : '100px',
    width : '200px',
    backgroundColor : 'coral'
}


export default LabRat;