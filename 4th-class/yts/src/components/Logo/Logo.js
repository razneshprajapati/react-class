import React from 'react';

const Logo=(props)=>{
    return(

        <div className={'logo'}>{props.label}</div>
    )
};

export default Logo;